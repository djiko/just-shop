package djiko.com.justshop

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.RelativeLayout

import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    private var isInShoppingMode: Boolean? = false

    private lateinit var mSharedPreferences: SharedPreferences

    private val mItems = ArrayList<String>()

    private var fab: FloatingActionButton? = null
    private var addingModeViews: RelativeLayout? = null

    private var mAutoCompleteAdapter: ArrayAdapter<String>? = null
    private var mItemAdapter: ItemAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        mSharedPreferences = getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE)
        isInShoppingMode = mSharedPreferences.getBoolean(SHOPPING_MODE_KEY, false)

        fab = findViewById(R.id.fab)
        fab!!.setOnClickListener {
            isInShoppingMode = !isInShoppingMode!!
            setShoppingModeViews()
        }
    }

    public override fun onPause() {
        super.onPause()
        saveToPrefs()
    }

    public override fun onResume() {
        super.onResume()

        loadFromPrefs()

        addingModeViews = findViewById(R.id.addingModeViews)

        mAutoCompleteAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, mItems)
        addItemAutocomplete!!.setAdapter(mAutoCompleteAdapter)
        addItemAutocomplete!!.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER)
                addItem(v)
            false
        }

        mItemAdapter = ItemAdapter(mItems)
        itemsList.layoutManager = LinearLayoutManager(this)
        itemsList.adapter = mItemAdapter

        val itemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT or ItemTouchHelper.DOWN or ItemTouchHelper.UP) {

            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                //Remove swiped item from list and notify the RecyclerView
                mItems.removeAt(viewHolder.adapterPosition)
                mItemAdapter!!.notifyDataSetChanged()
            }
        })
        itemTouchHelper.attachToRecyclerView(itemsList)
        setShoppingModeViews()
    }

    private fun setShoppingModeViews() {
        if (!isInShoppingMode!!) {
            hideKeyboard()
            addingModeViews!!.visibility = View.GONE
            fab!!.setImageResource(R.drawable.ic_add_shopping_cart_black_24dp)
        } else {
            addingModeViews!!.visibility = View.VISIBLE
            addItemAutocomplete!!.requestFocus()
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(addItemAutocomplete, InputMethodManager.SHOW_IMPLICIT)
            fab!!.setImageResource(R.drawable.ic_shopping_cart_black_24dp)
        }
    }

    fun addItem(@Suppress("UNUSED_PARAMETER")view: View) {
        val item = addItemAutocomplete!!.text.toString()
        if (item.trim { it <= ' ' }.isNotEmpty() && !mItems.contains(item)) {
            mItems.add(item)
            mItems.sort()
            mAutoCompleteAdapter!!.add(item)
            mAutoCompleteAdapter!!.sort { s1, s2 -> s1.compareTo(s2) }
            mItemAdapter!!.notifyDataSetChanged()
            saveToPrefs()
        }
        addItemAutocomplete!!.setText("")
    }

    private fun saveToPrefs() {
        val itemsSet = HashSet<String>()
        itemsSet.addAll(mItems)
        val editor = mSharedPreferences.edit()
        editor.putStringSet(ITEMS_KEY, itemsSet)
        editor.putBoolean(SHOPPING_MODE_KEY, isInShoppingMode!!)
        editor.apply()
    }

    private fun loadFromPrefs() {
        val items = mSharedPreferences.getStringSet(ITEMS_KEY, HashSet<String>()) as Collection<String>
        if (mItems.size > 0)
            mItems.clear()
        mItems.addAll(items)
        mItems.sort()
        isInShoppingMode = mSharedPreferences.getBoolean(SHOPPING_MODE_KEY, false)
    }

    companion object {

        private const val SHARED_PREFS_FILE = "djiko.com.justshop.prefs"
        private const val ITEMS_KEY = "items"
        private const val SHOPPING_MODE_KEY = "isInShoppingMode"

        fun Activity.hideKeyboard() {
            hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
        }

        private fun Context.hideKeyboard(view: View) {
            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}