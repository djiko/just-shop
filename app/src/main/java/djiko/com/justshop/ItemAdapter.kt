package djiko.com.justshop

import android.graphics.Paint.STRIKE_THRU_TEXT_FLAG
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class ItemAdapter internal constructor(private val mItems: ArrayList<String>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        internal var itemTextview: TextView = itemView.findViewById<View>(R.id.item_label) as TextView

        init {
            itemTextview.setOnClickListener { view ->
                val tv = view as TextView
                if (tv.paintFlags and STRIKE_THRU_TEXT_FLAG > 0)
                    tv.paintFlags = tv.paintFlags and STRIKE_THRU_TEXT_FLAG.inv()
                else
                    tv.paintFlags = tv.paintFlags or STRIKE_THRU_TEXT_FLAG
            }
        }

        override fun onClick(view: View) {}
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val itemRow = inflater.inflate(R.layout.item_row, parent, false)

        return ViewHolder(itemRow)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tv = holder.itemTextview
        tv.text = mItems[position]
        if (tv.paintFlags and STRIKE_THRU_TEXT_FLAG > 0)
            tv.paintFlags = tv.paintFlags and STRIKE_THRU_TEXT_FLAG.inv()
    }

    override fun getItemCount(): Int {
        return mItems.size
    }
}
